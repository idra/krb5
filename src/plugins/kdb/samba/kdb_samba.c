/* -*- mode: c; c-basic-offset: 4; indent-tabs-mode: nil -*- */
/*
 * plugins/kdb/samba/kdb_samba.c
 *
 * Copyright 2010 Red Hat, Inc.
 * All Rights Reserved.
 *
 *   Export of this software from the United States of America may
 *   require a specific license from the United States Government.
 *   It is the responsibility of any person or organization contemplating
 *   export to obtain such a license before exporting.
 *
 * WITHIN THAT CONSTRAINT, permission to use, copy, modify, and
 * distribute this software and its documentation for any purpose and
 * without fee is hereby granted, provided that the above copyright
 * notice appear in all copies and that both that copyright notice and
 * this permission notice appear in supporting documentation, and that
 * the name of M.I.T. not be used in advertising or publicity pertaining
 * to distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original M.I.T. software.
 * M.I.T. makes no representations about the suitability of
 * this software for any purpose.  It is provided "as is" without express
 * or implied warranty.
 *
 * Author: Simo Sorce <ssorce@redhat.com>
 */


#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <db.h>
#include <stdio.h>
#include <errno.h>
#include <utime.h>
#include "kdb5.h"
#include "kdb_samba.h"

#define MIT_SAMBA_LIBRARY "mit_samba"

static void
ks_context_free(struct ks_context *ks)
{
    if (!ks) return;

    krb5int_mutex_free(ks->lock);
    if (ks->fns) {
        ks->fns->fini(ks->ctx);
    }
}

static krb5_error_code
ks_init(void)
{
    return 0;
}

static krb5_error_code
ks_fini(void)
{
    return 0;
}

static krb5_error_code
ks_load_samba_module(krb5_context context,
                     struct ks_context *ks,
                     const char *libdir)
{
    struct errinfo errinfo;
    krb5_error_code code;
    char *libobj;
    int *version;
    int ret;

    ret = asprintf(&libobj, "%s/"MIT_SAMBA_LIBRARY"%s", libdir, SHLIBEXT);
    if (ret < 0) {
        return ENOMEM;
    }

    memset(&errinfo, 0, sizeof(errinfo));

    code = krb5int_open_plugin(libobj, &ks->pfh, &errinfo);
    if (code != 0) {
        goto done;
    }

    ret = krb5int_get_plugin_data(ks->pfh,
                                  "mit_samba_interface_version",
                                  (void **)&version, &errinfo);
    if (ret) {
        code = KRB5_KDB_BAD_VERSION;
        goto done;
    }
    if (*version != MIT_SAMBA_INTERFACE_VERSION) {
        code = KRB5_KDB_BAD_VERSION;
        goto done;
    }

    ret = krb5int_get_plugin_data(ks->pfh,
                                  "mit_samba_function_table",
                                  (void **)&ks->fns, &errinfo);
    if (ret) {
        code = KRB5_KDB_BAD_VERSION;
        goto done;
    }

done:
    /* TODO: free libobj ? */
    krb5int_free_error(&errinfo, NULL);
    return code;
}

static krb5_error_code
ks_init_module(krb5_context context,
               char *conf_section,
               char **db_args,
               int mode)
{
    kdb5_dal_handle *dal_handle = context->dal_handle;
    struct ks_context *ks;
    krb5_error_code code;
    char *libdir = NULL;

    if (dal_handle->db_context != NULL) {
        ks_context_free(dal_handle->db_context);
        dal_handle->db_context = NULL;
    }

    code = profile_get_string(context->profile, KDB_MODULE_SECTION,
                              conf_section, "samba_libdir", NULL, &libdir);
    if (code != 0) {
        return code;
    }

    ks = k5alloc(sizeof(struct ks_context), &code);
    if (code != 0) {
        goto done;
    }

    if (mode & KRB5_KDB_OPEN_RO) {
        ks->mode = O_RDONLY;
    } else {
        ks->mode = O_RDWR;
    }

    code = krb5int_mutex_alloc(&ks->lock);
    if (code != 0) {
        goto done;
    }

    code = ks_load_samba_module(context, ks, libdir);
    if (code != 0) {
        goto done;
    }

    code = ks->fns->init(&ks->ctx);
    if (code != 0) {
        goto done;
    }

    dal_handle->db_context = ks;

done:
    if (code != 0 && ks != NULL) {
        ks_context_free(ks);
    }
    return code;
}

static krb5_error_code
ks_fini_module(krb5_context context)
{
    kdb5_dal_handle *dal_handle = context->dal_handle;

    ks_context_free((struct ks_context *)dal_handle->db_context);
    dal_handle->db_context = NULL;

    return 0;
}

static krb5_error_code
ks_db_create(krb5_context context,
             char *conf_section,
             char **db_args)
{
    /* NOTE: used only by kadmin */
    return KRB5_KDB_DBTYPE_NOSUP;
}

static krb5_error_code
ks_db_destroy(krb5_context context,
              char *conf_section,
              char **db_args)
{
    /* NOTE: used only by kadmin */
    return KRB5_KDB_DBTYPE_NOSUP;
}

static krb5_error_code
ks_db_get_age(krb5_context context,
              char *db_name,
              time_t *age)
{
    /* TODO: returns last modification time of the db */
    /* NOTE: used by and affects only lookaside cache,
     *       defer implementation until needed as samba doesn't keep this
     *       specific value readily available and it would require a full
     *       database search to get it. */
    return KRB5_KDB_DBTYPE_NOSUP;
}

static krb5_error_code
ks_db_lock(krb5_context context, int kmode)
{

    /* NOTE: important only for kadmin */
    /* NOTE: deferred as samba's DB cannot be easily locked and doesn't
     * really make sense to do so anyway as the db is shared and support
     * transactions */
    return 0;
}

static krb5_error_code
ks_db_unlock(krb5_context context)
{

    /* NOTE: important only for kadmin */
    /* NOTE: deferred as samba's DB cannot be easily locked and doesn't
     * really make sense to do so anyway as the db is shared and support
     * transactions */
    return 0;
}

static krb5_error_code
ks_get_principal(krb5_context context,
                 krb5_const_principal principal,
                 unsigned int flags,
                 krb5_db_entry **kentry)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    char *principal_string = NULL;
    hdb_entry_ex *hentry = NULL;
    krb5_error_code code;
    int error;

    code = krb5_unparse_name(context, principal, &principal_string);
    if (code != 0) {
        goto cleanup;
    }

    error = KS_GET_PRINCIPAL(ks, principal_string, flags, &hentry);
    code = ks_map_error(error);
    if (code != 0) {
        goto cleanup;
    }

    code = ks_unmarshal_hdb_entry(context, hentry, kentry);
    if (code != 0) {
        goto cleanup;
    }

cleanup:

    if (principal_string) {
        krb5_free_unparsed_name(context, principal_string);
    }
    if (code != 0) {
        if (hentry) {
            KS_FREE_DB_ENTRY(ks, hentry);
        }
    }

    return code;
}

static krb5_boolean
ks_is_master_key_principal(krb5_context context,
                           krb5_const_principal princ)
{
    return krb5_princ_size(context, princ) == 2 &&
        data_eq_string(princ->data[0], "K") &&
        data_eq_string(princ->data[1], "M");
}

static krb5_error_code
ks_is_tgs_principal(krb5_context context,
                    krb5_const_principal princ)
{
    return krb5_princ_size(context, princ) == 2 &&
        data_eq_string(princ->data[0], KRB5_TGS_NAME);
}

static krb5_error_code
ks_get_master_key_principal(krb5_context context,
                            krb5_const_principal princ,
                            krb5_db_entry **kentry_ptr)
{
    krb5_error_code code;
    krb5_key_data *key_data;
    krb5_timestamp now;
    krb5_db_entry *kentry;

    *kentry_ptr = NULL;

    kentry = k5alloc(sizeof(*kentry), &code);
    if (kentry == NULL) {
        return code;
    }

    kentry->magic = KRB5_KDB_MAGIC_NUMBER;
    kentry->len = KRB5_KDB_V1_BASE_LENGTH;
    kentry->attributes = KRB5_KDB_DISALLOW_ALL_TIX;

    if (princ == NULL) {
        code = krb5_parse_name(context, KRB5_KDB_M_NAME, &kentry->princ);
    } else {
        code = krb5_copy_principal(context, princ, &kentry->princ);
    }
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
        return code;
    }

    now = time(NULL);

    code = krb5_dbe_update_mod_princ_data(context, kentry, now, kentry->princ);
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
        return code;
    }

    /* Return a dummy key */
    kentry->n_key_data = 1;
    kentry->key_data = k5alloc(sizeof(krb5_key_data), &code);
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
        return code;
    }

    key_data = &kentry->key_data[0];

    key_data->key_data_ver          = KRB5_KDB_V1_KEY_DATA_ARRAY;
    key_data->key_data_kvno         = 1;
    key_data->key_data_type[0]      = ENCTYPE_UNKNOWN;
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
        return code;
    }

    *kentry_ptr = kentry;

    return 0;
}

static krb5_boolean
ks_is_kadmin_history(krb5_context context,
                     krb5_const_principal princ)
{
    return krb5_princ_size(context, princ) == 2 &&
        data_eq_string(princ->data[0], "kadmin") &&
        data_eq_string(princ->data[1], "history");
}

static krb5_error_code
ks_get_dummy_principal(krb5_context context,
                       krb5_const_principal princ,
                       krb5_db_entry **kentry_ptr)
{
    krb5_error_code code;
    krb5_key_data *key_data;
    krb5_timestamp now;
    krb5_db_entry *kentry;
    krb5_keyblock key;
    krb5_data salt;
    krb5_data pwd;
    int enctype = ENCTYPE_AES256_CTS_HMAC_SHA1_96;
    int sts = KRB5_KDB_SALTTYPE_SPECIAL;

    if (princ == NULL) {
        return KRB5_KDB_NOENTRY;
    }

    *kentry_ptr = NULL;

    kentry = k5alloc(sizeof(*kentry), &code);
    if (kentry == NULL) {
        return code;
    }

    kentry->magic = KRB5_KDB_MAGIC_NUMBER;
    kentry->len = KRB5_KDB_V1_BASE_LENGTH;

    code = krb5_copy_principal(context, princ, &kentry->princ);
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
        return code;
    }

    now = time(NULL);

    code = krb5_dbe_update_mod_princ_data(context, kentry, now, kentry->princ);
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
        return code;
    }

    /* Return a dummy key */
    salt.data = "salt";
    salt.length = 4;

    pwd.data = "secret";
    pwd.length = 5;

    code = krb5_c_string_to_key(context, enctype, &pwd, &salt, &key);
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
        return code;
    }

    kentry->n_key_data = 1;
    kentry->key_data = k5alloc(sizeof(krb5_key_data), &code);
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
        return code;
    }

    key_data = &kentry->key_data[0];

    key_data->key_data_ver          = KRB5_KDB_V1_KEY_DATA_ARRAY;
    key_data->key_data_kvno         = 1;
    key_data->key_data_type[0]      = key.enctype;
    key_data->key_data_length[0]    = key.length;
    key_data->key_data_contents[0]  = key.contents;
    key_data->key_data_type[1]      = sts;
    key_data->key_data_length[1]    = salt.length;
    key_data->key_data_contents[1]  = (krb5_octet *)strdup("salt");

    *kentry_ptr = kentry;

    return 0;
}

static krb5_error_code
ks_db_get_principal(krb5_context context,
                    krb5_const_principal princ,
                    unsigned int kflags,
                    krb5_db_entry **kentry)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    krb5_error_code code;
    unsigned int hflags;

    if (ks == NULL) {
        return KRB5_KDB_DBNOTINITED;
    }

    if (ks_is_master_key_principal(context, princ)) {
        return ks_get_master_key_principal(context, princ, kentry);
    }

    /* FIXME: temporarily fake up kadmin history to let kadmin.local work */
    if (ks_is_kadmin_history(context, princ)) {
        return ks_get_dummy_principal(context, princ, kentry);
    }

    code = k5_mutex_lock(ks->lock);
    if (code != 0) {
        return code;
    }

    hflags = 0;
    if (kflags & KRB5_KDB_FLAG_CANONICALIZE)
        hflags |= HDB_F_CANON;
    if (kflags & (KRB5_KDB_FLAG_CLIENT_REFERRALS_ONLY |
                  KRB5_KDB_FLAG_INCLUDE_PAC))
        hflags |= HDB_F_GET_CLIENT;
    else if (ks_is_tgs_principal(context, princ))
        hflags |= HDB_F_GET_KRBTGT;
    else
        hflags |= HDB_F_GET_ANY;

    /* always set this or the created_by data will not be populated by samba's
     * backend and we will fail to parse the entry later */
    hflags |= HDB_F_ADMIN_DATA;

    code = ks_get_principal(context, princ, hflags, kentry);

    k5_mutex_unlock(ks->lock);

    return code;
}

static void
ks_db_free_principal(krb5_context context,
                     krb5_db_entry *entry)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    krb5_error_code code;

    if (!ks) {
        return;
    }

    code = k5_mutex_lock(ks->lock);
    if (code != 0) {
        return;
    }

    ks_free_krb5_db_entry(context, entry);

    k5_mutex_unlock(ks->lock);
}

static krb5_error_code
ks_db_put_principal(krb5_context context,
                    krb5_db_entry *entry,
                    char **db_args)
{

    /* NOTE: deferred, samba does not allow the KDC to store
     * principals for now */
    return KRB5_KDB_DB_INUSE;
}

static krb5_error_code
ks_db_delete_principal(krb5_context context,
                       krb5_const_principal princ)
{

    /* NOTE: deferred, samba does not allow the KDC to delete
     * principals for now */
    return KRB5_KDB_DB_INUSE;
}

static krb5_error_code
ks_db_iterate(krb5_context context,
              char *match_entry,
              int (*func)(krb5_pointer, krb5_db_entry *),
              krb5_pointer func_arg)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    hdb_entry_ex *hentry = NULL;
    krb5_error_code code;
    int error;

    if (!ks) {
        return KRB5_KDB_DBNOTINITED;
    }

    code = k5_mutex_lock(ks->lock);
    if (code != 0) {
        return code;
    }

    error = KS_GET_FIRSTKEY(ks, &hentry);
    code = ks_map_error(error);

    while (code == 0) {
        krb5_db_entry *kentry;

        code = ks_unmarshal_hdb_entry(context, hentry, &kentry);
        if (code == 0) {
            code = (*func)(func_arg, kentry);
            ks_free_krb5_db_entry(context, kentry);
        }

        if (code != 0) {
            break;
        }

        error = KS_GET_NEXTKEY(ks, &hentry);
        code = ks_map_error(error);
    }

    if (code == KRB5_KDB_NOENTRY) {
        code = 0;
    }

    k5_mutex_unlock(ks->lock);
    return code;
}

static void *
ks_db_alloc(krb5_context context, void *ptr, size_t size)
{
    return realloc(ptr, size);
}

static void
ks_db_free(krb5_context context, void *ptr)
{
    free(ptr);
}

static krb5_error_code
ks_fetch_master_key(krb5_context context,
                    krb5_principal name,
                    krb5_keyblock *key,
                    krb5_kvno *kvno,
                    char *db_args)
{
    return 0;
}

static krb5_error_code
ks_fetch_master_key_list(krb5_context context,
                         krb5_principal mname,
                         const krb5_keyblock *key,
                         krb5_kvno kvno,
                         krb5_keylist_node **mkeys_list)
{
    krb5_keylist_node *mkey;
    krb5_error_code code;

    /* NOTE: samba does not support master keys
     *       so just return a dummy key */
    mkey = k5alloc(sizeof(*mkey), &code);
    if (code != 0)
        return code;

    mkey->keyblock.magic = KV5M_KEYBLOCK;
    mkey->keyblock.enctype = ENCTYPE_UNKNOWN;
    mkey->kvno = 1;

    *mkeys_list = mkey;

    return 0;
}

static krb5_error_code
ks_promote_db(krb5_context context,
              char *conf_section,
              char **db_args)
{

    /* NOTE: there is nothing to be promoted in a samba db. */
    return KRB5_KDB_DBTYPE_NOSUP;
}

static krb5_error_code
ks_dbekd_decrypt_key_data(krb5_context context,
                          const krb5_keyblock *mkey,
                          const krb5_key_data *key_data,
                          krb5_keyblock *kkey,
                          krb5_keysalt *keysalt)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    krb5_error_code code;

    if (!ks) {
        return KRB5_KDB_DBNOTINITED;
    }

    /* NOTE: samba doesn't use a master key, so we will just copy
     * the contents around untouched. */
    memset(kkey, 0, sizeof(krb5_keyblock));
    kkey->magic = KV5M_KEYBLOCK;
    kkey->enctype = key_data->key_data_type[0];
    kkey->contents = k5alloc(key_data->key_data_length[0], &code);
    if (code != 0) {
        return code;
    }
    memcpy(kkey->contents,
           key_data->key_data_contents[0],
           key_data->key_data_length[0]);
    kkey->length = key_data->key_data_length[0];

    if (keysalt) {
        keysalt->type = key_data->key_data_type[1];
        keysalt->data.data = k5alloc(key_data->key_data_length[1], &code);
        if (code != 0) {
            free(kkey->contents);
            return code;
        }
        memcpy(keysalt->data.data,
               key_data->key_data_contents[1],
               key_data->key_data_length[1]);
        keysalt->data.length = key_data->key_data_length[1];
    }

    return 0;
}

static krb5_error_code
ks_dbekd_encrypt_key_data(krb5_context context,
                          const krb5_keyblock *mkey,
                          const krb5_keyblock *kkey,
                          const krb5_keysalt *keysalt,
                          int keyver,
                          krb5_key_data *key_data)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    krb5_error_code code;

    if (!ks) {
        return KRB5_KDB_DBNOTINITED;
    }

    /* NOTE: samba doesn't use a master key, so we will just copy
     * the contents around untouched. */
    memset(key_data, 0, sizeof(krb5_key_data));
    key_data->key_data_ver = KRB5_KDB_V1_KEY_DATA_ARRAY;
    key_data->key_data_kvno = keyver;
    key_data->key_data_type[0] = kkey->enctype;
    key_data->key_data_contents[0] = k5alloc(kkey->length, &code);
    if (code != 0) {
        return code;
    }
    memcpy(key_data->key_data_contents[0],
                kkey->contents, kkey->length);
    key_data->key_data_length[0] = kkey->length;

    if (keysalt) {
        key_data->key_data_type[1] = keysalt->type;
        key_data->key_data_contents[1] = k5alloc(keysalt->data.length, &code);
        if (code != 0) {
            free(key_data->key_data_contents[0]);
            return code;
        }
        memcpy(key_data->key_data_contents[1],
               keysalt->data.data, keysalt->data.length);
        key_data->key_data_length[1] = keysalt->data.length;
    }

    return 0;
}

kdb_vftabl kdb_function_table = {
    KRB5_KDB_DAL_MAJOR_VERSION,
    0,
    ks_init,
    ks_fini,
    ks_init_module,
    ks_fini_module,
    ks_db_create,
    ks_db_destroy,
    ks_db_get_age,
    ks_db_lock,
    ks_db_unlock,
    ks_db_get_principal,
    ks_db_free_principal,
    ks_db_put_principal,
    ks_db_delete_principal,
    ks_db_iterate,
    NULL, /* create_policy */
    NULL, /* get_policy */
    NULL, /* put_policy */
    NULL, /* iter_policy */
    NULL, /* delete_policy */
    NULL, /* free_policy */
    ks_db_alloc,
    ks_db_free,
    ks_fetch_master_key,
    ks_fetch_master_key_list,
    NULL, /* store_master_key_list */
    NULL, /* dbe_search_enctype */
    NULL, /* change_pwd */
    ks_promote_db,
    ks_dbekd_decrypt_key_data,
    ks_dbekd_encrypt_key_data,
    ks_db_sign_auth_data,
    NULL, /* check_transited_realms */
    ks_db_check_policy_as,
    NULL, /* check_policy_tgs */
    NULL, /* audit_as_req */
    NULL, /* refresh_config */
    ks_db_check_allowed_to_delegate
};
