/* -*- mode: c; c-basic-offset: 4; indent-tabs-mode: nil -*- */
/*
 * plugins/kdb/samba/kdb_samba_policy.c
 *
 * Copyright 2010 Red Hat, Inc.
 * All Rights Reserved.
 *
 *   Export of this software from the United States of America may
 *   require a specific license from the United States Government.
 *   It is the responsibility of any person or organization contemplating
 *   export to obtain such a license before exporting.
 *
 * WITHIN THAT CONSTRAINT, permission to use, copy, modify, and
 * distribute this software and its documentation for any purpose and
 * without fee is hereby granted, provided that the above copyright
 * notice appear in all copies and that both that copyright notice and
 * this permission notice appear in supporting documentation, and that
 * the name of M.I.T. not be used in advertising or publicity pertaining
 * to distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original M.I.T. software.
 * M.I.T. makes no representations about the suitability of
 * this software for any purpose.  It is provided "as is" without express
 * or implied warranty.
 *
 * Author: Simo Sorce <ssorce@redhat.com>
 */

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <db.h>
#include <stdio.h>
#include <errno.h>
#include <utime.h>
#include "kdb5.h"
#include "kdb_samba.h"

krb5_error_code
ks_db_check_allowed_to_delegate(krb5_context context,
                                krb5_const_principal client,
                                const krb5_db_entry *server,
                                krb5_const_principal proxy)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    hdb_entry_ex *delegating_service;
    char *target_name = NULL;
    bool is_enterprise;
    krb5_error_code code;
    int error;

/*
 *  Names are quite odd and confusing in the current implementation.
 *  The following mappings should help understanding what is what.
 *  client ->  client to impersonate
 *  server; -> delegating service
 *  proxy; -> target principal
*/

    delegating_service = (hdb_entry_ex *)server->e_data;

    code = krb5_unparse_name(context, proxy, &target_name);
    if (code) {
        goto done;
    }

    is_enterprise = (proxy->type == KRB5_NT_ENTERPRISE_PRINCIPAL);

    error = KS_CHECK_S4U2PROXY(ks, delegating_service,
                               target_name, is_enterprise);
    code = ks_map_error(error);

done:
    free(target_name);
    return code;
}

static krb5_error_code
get_netbios_name(krb5_address **addrs, char **name)
{
    char *nb_name = NULL;
    int len, i;

    for (i = 0; addrs[i]; i++) {
        if (addrs[i]->addrtype != ADDRTYPE_NETBIOS) {
            continue;
        }
        len = MIN(addrs[i]->length, 15);
        nb_name = strndup((const char *)addrs[i]->contents, len);
        if (!nb_name) {
            return ENOMEM;
        }
        break;
    }

    if (nb_name) {
        /* Strip space padding */
        i = strlen(nb_name) - 1;
        while (i > 0 && nb_name[i] == ' ') {
            nb_name[i] = '\0';
        }
    }

    *name = nb_name;

    return 0;
}

krb5_error_code
ks_db_check_policy_as(krb5_context context,
                      krb5_kdc_req *kdcreq,
                      krb5_db_entry *client_dbe,
                      krb5_db_entry *server_dbe,
                      krb5_timestamp kdc_time,
                      const char **status,
                      krb5_data *e_data)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    krb5_error_code code;
    hdb_entry_ex *client;
    hdb_entry_ex *server;
    char *client_name = NULL;
    char *server_name = NULL;
    char *netbios_name = NULL;
    char *realm = NULL;
    bool password_change = false;
    DATA_BLOB int_data = { NULL, 0 };
    int error;

    server = (hdb_entry_ex *)server_dbe->e_data;
    client = (hdb_entry_ex *)client_dbe->e_data;

    if (krb5_princ_size(context, kdcreq->server) == 2 &&
        data_eq_string(kdcreq->server->data[0], "kadmin") &&
        data_eq_string(kdcreq->server->data[1], "changepw")) {

        code = krb5_get_default_realm(context, &realm);
        if (code) {
            goto done;
        }

        if (data_eq_string(kdcreq->server->realm, realm)) {
            password_change = true;
        }
    }

    code = krb5_unparse_name(context, kdcreq->server, &server_name);
    if (code) {
        goto done;
    }

    code = krb5_unparse_name(context, kdcreq->client, &client_name);
    if (code) {
        goto done;
    }

    if (kdcreq->addresses) {
        code = get_netbios_name(kdcreq->addresses, &netbios_name);
        if (code) {
            goto done;
        }
    }

    error = KS_CLIENT_ACCESS(ks,
                             client, client_name,
                             server, server_name,
                             netbios_name,
                             password_change,
                             &int_data);
    code = ks_map_error(error);
    if (code) {
        goto done;
    }

    *e_data = make_data(int_data.data, int_data.length);

done:
    free(realm);
    free(server_name);
    free(client_name);
    free(netbios_name);

    return code;
}

static krb5_error_code
ks_get_pac(krb5_context context,
           krb5_db_entry *client,
           krb5_pac *pac)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    hdb_entry_ex *hentry = (hdb_entry_ex *)client->e_data;
    DATA_BLOB pac_data;
    krb5_data data;
    krb5_error_code code;
    int error;

    error = KS_GET_PAC(ks, hentry, &pac_data);
    code = ks_map_error(error);
    if (code != 0) {
        return code;
    }

    code = krb5_pac_init(context, pac);
    if (code != 0) {
        goto done;
    }

    data = make_data(pac_data.data, pac_data.length);

    code = krb5_pac_add_buffer(context, *pac, PAC_LOGON_INFO, &data);
    if (code != 0) {
        goto done;
    }

done:
    free(pac_data.data);
    return code;
}

static krb5_error_code
ks_verify_pac(krb5_context context,
              unsigned int flags,
              krb5_const_principal client_princ,
              krb5_db_entry *client,
              krb5_keyblock *server_key,
              krb5_keyblock *krbtgt_key,
              krb5_timestamp authtime,
              krb5_authdata **tgt_auth_data,
              krb5_pac *pac)
{
    struct ks_context *ks = GET_KS_CONTEXT(context);
    hdb_entry_ex *hentry = NULL;
    krb5_authdata **authdata = NULL;
    krb5_pac ipac = NULL;
    DATA_BLOB pac_data = { NULL, 0 };
    DATA_BLOB logon_data = { NULL, 0 };
    krb5_data data;
    krb5_error_code code;
    int error;

    /* find the existing PAC, if present */
    code = krb5int_find_authdata(context,
                                 tgt_auth_data, NULL,
                                 KRB5_AUTHDATA_WIN2K_PAC,
                                 &authdata);
    if (code != 0) {
        return code;
    }

    /* no pac data */
    if (authdata == NULL) {
        return 0;
    }

    assert(authdata[0] != NULL);

    if (authdata[1] != NULL) {
        code = KRB5KDC_ERR_BADOPTION; /* XXX */
        goto done;
    }

    code = krb5_pac_parse(context,
                          authdata[0]->contents,
                          authdata[0]->length,
                          &ipac);
    if (code != 0) {
        goto done;
    }

    /* TODO: verify this is correct
     *
     * In the constrained delegation case, the PAC is from a service
     * ticket rather than a TGT; we must verify the server and KDC
     * signatures to assert that the server did not forge the PAC.
     */
    if (flags & KRB5_KDB_FLAG_CONSTRAINED_DELEGATION) {
        code = krb5_pac_verify(context, ipac, authtime,
                               client_princ,
                               server_key, krbtgt_key);
    } else {
        code = krb5_pac_verify(context, ipac, authtime,
                               client_princ,
                               krbtgt_key, NULL);
    }
    if (code != 0) {
        goto done;
    }

    /* check and update PAC */
    if (client) {
        hentry = (hdb_entry_ex *)client->e_data;
    }

    pac_data.data = authdata[0]->contents;
    pac_data.length = authdata[0]->length;

    error = KS_UPDATE_PAC(ks, hentry, &pac_data, &logon_data);
    code = ks_map_error(error);
    if (code != 0) {
        goto done;
    }

    code = krb5_pac_init(context, pac);
    if (code != 0) {
        goto done;
    }

    data = make_data(logon_data.data, logon_data.length);

    code = krb5_pac_add_buffer(context, *pac, PAC_LOGON_INFO, &data);
    if (code != 0) {
        goto done;
    }

done:
    krb5_free_authdata(context, authdata);
    krb5_pac_free(context, ipac);
    free(logon_data.data);
    return code;
}

krb5_error_code
ks_db_sign_auth_data(krb5_context context,
                     unsigned int flags,
                     krb5_const_principal client_princ,
                     krb5_db_entry *client,
                     krb5_db_entry *server,
                     krb5_db_entry *krbtgt,
                     krb5_keyblock *client_key,
                     krb5_keyblock *server_key,
                     krb5_keyblock *krbtgt_key,
                     krb5_keyblock *session_key,
                     krb5_timestamp authtime,
                     krb5_authdata **tgt_auth_data,
                     krb5_authdata ***signed_auth_data)
{
    krb5_const_principal ks_client_princ;
    krb5_authdata **authdata = NULL;
    krb5_boolean is_as_req;
    krb5_error_code code;
    krb5_pac pac = NULL;
    krb5_data pac_data;

    /* Prefer canonicalised name from client entry */
    if (client != NULL) {
        ks_client_princ = client->princ;
    } else {
        ks_client_princ = client_princ;
    }

    is_as_req = ((flags & KRB5_KDB_FLAG_CLIENT_REFERRALS_ONLY) != 0);

    if (is_as_req && (flags & KRB5_KDB_FLAG_INCLUDE_PAC)) {

        code = ks_get_pac(context, client, &pac);
        if (code != 0) {
            goto done;
        }
    }

    if (!is_as_req) {
        code = ks_verify_pac(context, flags, ks_client_princ, client,
                             server_key, krbtgt_key, authtime,
                             tgt_auth_data, &pac);
        if (code != 0) {
            goto done;
        }
    }

    if (pac == NULL && client != NULL) {

        code = ks_get_pac(context, client, &pac);
        if (code != 0) {
            goto done;
        }
    }

    if (pac == NULL) {
        code = KRB5_KDB_DBTYPE_NOSUP;
        goto done;
    }

    code = krb5int_pac_sign(context, pac, authtime, ks_client_princ,
                            server_key, krbtgt_key, &pac_data);
    if (code != 0) {
        goto done;
    }

    authdata = k5alloc(2 * sizeof(krb5_authdata *), &code);
    if (code != 0) {
        goto done;
    }
    authdata[0] = k5alloc(sizeof(krb5_authdata), &code);
    if (code != 0) {
        goto done;
    }
    /* put in signed data */
    authdata[0]->magic = KV5M_AUTHDATA;
    authdata[0]->ad_type = KRB5_AUTHDATA_WIN2K_PAC;
    authdata[0]->contents = (krb5_octet *)pac_data.data;
    authdata[0]->length = pac_data.length;

    code = krb5_encode_authdata_container(context,
                                          KRB5_AUTHDATA_IF_RELEVANT,
                                          authdata,
                                          signed_auth_data);
    if (code != 0) {
        goto done;
    }

    code = 0;

done:
    krb5_pac_free(context, pac);
    krb5_free_authdata(context, authdata);
    return code;
}
