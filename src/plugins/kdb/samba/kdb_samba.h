/* -*- mode: c; c-basic-offset: 4; indent-tabs-mode: nil -*- */
/*
 * plugins/kdb/samba/kdb_samba.h
 *
 * Copyright (c) 2009, Simo Sorce <idra@samba.org>
 * All Rights Reserved.
 *
 *   Export of this software from the United States of America may
 *   require a specific license from the United States Government.
 *   It is the responsibility of any person or organization contemplating
 *   export to obtain such a license before exporting.
 *
 * WITHIN THAT CONSTRAINT, permission to use, copy, modify, and
 * distribute this software and its documentation for any purpose and
 * without fee is hereby granted, provided that the above copyright
 * notice appear in all copies and that both that copyright notice and
 * this permission notice appear in supporting documentation, and that
 * the name of M.I.T. not be used in advertising or publicity pertaining
 * to distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original M.I.T. software.
 * M.I.T. makes no representations about the suitability of
 * this software for any purpose.  It is provided "as is" without express
 * or implied warranty.
 *
 */

#ifndef _KDB_SAMBA_H_
#define _KDB_SAMBA_H_

#include "k5-plugin.h"
#include "hdb_asn1.h"

/* flags for various functions */
#define HDB_F_DECRYPT		1	/* decrypt keys */
#define HDB_F_REPLACE		2	/* replace entry */
#define HDB_F_GET_CLIENT	4	/* fetch client */
#define HDB_F_GET_SERVER	8	/* fetch server */
#define HDB_F_GET_KRBTGT	16	/* fetch krbtgt */
#define HDB_F_GET_ANY		28	/* fetch any of client,server,krbtgt */
#define HDB_F_CANON		32	/* want canonicalition */
#define HDB_F_ADMIN_DATA        64      /* want data that kdc don't use  */
#define HDB_F_KVNO_SPECIFIED    128     /* we want a particular KVNO */

#define HDB_ERR_UK_SERROR 36150273
#define HDB_ERR_UK_RERROR 36150274
#define HDB_ERR_NOENTRY 36150275
#define HDB_ERR_DB_INUSE 36150276
#define HDB_ERR_DB_CHANGED 36150277
#define HDB_ERR_RECURSIVELOCK 36150278
#define HDB_ERR_NOTLOCKED 36150279
#define HDB_ERR_BADLOCKMODE 36150280
#define HDB_ERR_CANT_LOCK_DB 36150281
#define HDB_ERR_EXISTS 36150282
#define HDB_ERR_BADVERSION 36150283
#define HDB_ERR_NO_MKEY 36150284
#define HDB_ERR_MANDATORY_OPTION 36150285

#define PAC_LOGON_INFO 1

typedef struct hdb_entry_ex {
    void *ctx;
    hdb_entry entry;
    void (*free_entry)(void *, struct hdb_entry_ex *);
} hdb_entry_ex;

/* NOTE: Relies on the fact that samba doesn't use the context
 * for free_entry(), that's why we declare it (void *) */
#define KS_FREE_DB_ENTRY(ks_ctx, hentry) (hentry)->free_entry(NULL, (hentry));

#include <stdbool.h>
#include "mit_samba_interface.h"

struct ks_context {
    k5_mutex_t *lock;
    int mode;

    /* mit_samba library APIs */
    struct plugin_file_handle *pfh;
    struct mit_samba_context *ctx;
    struct mit_samba_function_table *fns;
};

#define GET_KS_CONTEXT(krb5_ctx) \
    ((struct ks_context *)((krb5_ctx)->dal_handle->db_context))

#define KS_GET_PRINCIPAL(ks, str, flags, ptr) \
    (ks)->fns->get_principal((ks)->ctx, str, flags, ptr)

#define KS_GET_FIRSTKEY(ks, pptr) \
    (ks)->fns->get_firstkey((ks)->ctx, pptr)
#define KS_GET_NEXTKEY(ks, pptr) \
    (ks)->fns->get_nextkey((ks)->ctx, pptr)

#define KS_GET_PAC(ks, cli, ptr) \
    (ks)->fns->get_pac((ks)->ctx, cli, ptr)
#define KS_UPDATE_PAC(ks, cli, ptr1, ptr2) \
    (ks)->fns->update_pac((ks)->ctx, cli, ptr1, ptr2)

#define KS_CLIENT_ACCESS(ks, cli, clin, srv, srvn, nbn, pwc, ptr) \
    (ks)->fns->client_access((ks)->ctx, cli, clin, srv, srvn, nbn, pwc, ptr)
#define KS_CHECK_S4U2PROXY(ks, ds, tn, ise) \
    (ks)->fns->check_s4u2proxy((ks)->ctx, ds, tn, ise)

/* from kdb_samba_util.c */

krb5_error_code
ks_map_error(int error);

void
ks_free_krb5_db_entry(krb5_context context,
                      krb5_db_entry *entry);

krb5_error_code
ks_unmarshal_Principal(krb5_context context,
                       const Principal *hprinc,
                       krb5_principal *out_kprinc);

krb5_error_code
ks_unmarshal_hdb_entry(krb5_context context,
                       struct hdb_entry_ex *hentry,
                       krb5_db_entry **kentry_ptr);

/* from kdb_samba_policies.c */

krb5_error_code
ks_db_check_allowed_to_delegate(krb5_context context,
                                krb5_const_principal client,
                                const krb5_db_entry *server,
                                krb5_const_principal proxy);

krb5_error_code
ks_db_check_policy_as(krb5_context context,
                      krb5_kdc_req *kdcreq,
                      krb5_db_entry *client_dbe,
                      krb5_db_entry *server_dbe,
                      krb5_timestamp kdc_time,
                      const char **status,
                      krb5_data *e_data);

krb5_error_code
ks_db_sign_auth_data(krb5_context context,
                     unsigned int flags,
                     krb5_const_principal client_princ,
                     krb5_db_entry *client,
                     krb5_db_entry *server,
                     krb5_db_entry *krbtgt,
                     krb5_keyblock *client_key,
                     krb5_keyblock *server_key,
                     krb5_keyblock *krbtgt_key,
                     krb5_keyblock *session_key,
                     krb5_timestamp authtime,
                     krb5_authdata **tgt_auth_data,
                     krb5_authdata ***signed_auth_data);

#endif /* _KDB_SAMBA_H_ */
