/* -*- mode: c; c-basic-offset: 4; indent-tabs-mode: nil -*- */
/*
 * plugins/kdb/samba/kdb_samba_util.c
 *
 * Copyright 2009 by the Massachusetts Institute of Technology.
 * All Rights Reserved.
 *
 * Export of this software from the United States of America may
 *   require a specific license from the United States Government.
 *   It is the responsibility of any person or organization contemplating
 *   export to obtain such a license before exporting.
 *
 * WITHIN THAT CONSTRAINT, permission to use, copy, modify, and
 * distribute this software and its documentation for any purpose and
 * without fee is hereby granted, provided that the above copyright
 * notice appear in all copies and that both that copyright notice and
 * this permission notice appear in supporting documentation, and that
 * the name of M.I.T. not be used in advertising or publicity pertaining
 * to distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original M.I.T. software.
 * M.I.T. makes no representations about the suitability of
 * this software for any purpose.  It is provided "as is" without express
 * or implied warranty.
 *
 */

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <db.h>
#include <stdio.h>
#include <errno.h>
#include <utime.h>
#include "kdb5.h"
#include "kdb_samba.h"

krb5_error_code
ks_map_error(int error)
{
    krb5_error_code code;

    switch (error) {
    case HDB_ERR_UK_SERROR:
        code = KRB5_KDB_UK_SERROR;
        break;
    case HDB_ERR_UK_RERROR:
        code = KRB5_KDB_UK_RERROR;
        break;
    case HDB_ERR_NOENTRY:
        code = KRB5_KDB_NOENTRY;
        break;
    case HDB_ERR_DB_INUSE:
        code = KRB5_KDB_DB_INUSE;
        break;
    case HDB_ERR_DB_CHANGED:
        code = KRB5_KDB_DB_CHANGED;
        break;
    case HDB_ERR_RECURSIVELOCK:
        code = KRB5_KDB_RECURSIVELOCK;
        break;
    case HDB_ERR_NOTLOCKED:
        code = KRB5_KDB_NOTLOCKED;
        break;
    case HDB_ERR_BADLOCKMODE:
        code = KRB5_KDB_BADLOCKMODE;
        break;
    case HDB_ERR_CANT_LOCK_DB:
        code = KRB5_KDB_CANTLOCK_DB;
        break;
    case HDB_ERR_EXISTS:
        code = EEXIST;
        break;
    case HDB_ERR_BADVERSION:
        code = KRB5_KDB_BAD_VERSION;
        break;
    case HDB_ERR_NO_MKEY:
        code = KRB5_KDB_NOMASTERKEY;
        break;
    case HDB_ERR_MANDATORY_OPTION:
        code = KRB5_KDB_DBTYPE_NOSUP;
        break;
    default:
        code = error;
        break;
    }

    return code;
}

void
ks_free_krb5_db_entry(krb5_context context,
                      krb5_db_entry *entry)
{
    krb5_tl_data *tl_data_next = NULL;
    krb5_tl_data *tl_data = NULL;
    int i, j;

    if (entry == NULL) {
        return;
    }

    if (entry->e_data) {
        KS_FREE_DB_ENTRY(ks, (struct hdb_entry_ex *)(entry->e_data));
    }

    krb5_free_principal(context, entry->princ);

    for (tl_data = entry->tl_data; tl_data; tl_data = tl_data_next) {
        tl_data_next = tl_data->tl_data_next;
        if (tl_data->tl_data_contents != NULL)
            free(tl_data->tl_data_contents);
        free(tl_data);
    }

    if (entry->key_data != NULL) {
        for (i = 0; i < entry->n_key_data; i++) {
            for (j = 0; j < entry->key_data[i].key_data_ver; j++) {
                if (entry->key_data[i].key_data_length[j] != 0) {
                    if (entry->key_data[i].key_data_contents[j] != NULL) {
                        memset(entry->key_data[i].key_data_contents[j],
                               0,
                               entry->key_data[i].key_data_length[j]);
                        free(entry->key_data[i].key_data_contents[j]);
                    }
                }
                entry->key_data[i].key_data_contents[j] = NULL;
                entry->key_data[i].key_data_length[j] = 0;
                entry->key_data[i].key_data_type[j] = 0;
            }
        }
        free(entry->key_data);
    }

    free(entry);
}

#if 0
static krb5_error_code
ks_unmarshal_octet_string_contents(krb5_context context,
                                   const heim_octet_string *in_data,
                                   krb5_data *out_data)
{
    out_data->magic = KV5M_DATA;
    out_data->data = malloc(in_data->length);
    if (out_data->data == NULL)
        return ENOMEM;

    memcpy(out_data->data, in_data->data, in_data->length);

    out_data->length = in_data->length;

    return 0;
}

static krb5_error_code
ks_unmarshal_octet_string(krb5_context context,
                          heim_octet_string *in_data,
                          krb5_data **out_data)
{
    krb5_error_code code;

    *out_data = k5alloc(sizeof(krb5_data), &code);
    if (code != 0)
        return code;

    code = ks_unmarshal_octet_string_contents(context, in_data, *out_data);
    if (code != 0) {
        free(*out_data);
        *out_data = NULL;
        return code;
    }

    return 0;
}
#endif

static krb5_error_code
ks_unmarshal_general_string_contents(krb5_context context,
                                     const heim_general_string in_str,
                                     krb5_data *out_data)
{
    out_data->magic = KV5M_DATA;
    out_data->length = strlen(in_str);
    out_data->data = malloc(out_data->length);
    if (out_data->data == NULL)
        return ENOMEM;

    memcpy(out_data->data, in_str, out_data->length);
    return 0;
}

#if 0
static krb5_error_code
ks_unmarshal_general_string(krb5_context context,
                            const heim_general_string in_str,
                            krb5_data **out_data)
{
    krb5_error_code code;

    *out_data = k5alloc(sizeof(krb5_data), &code);
    if (code != 0)
        return code;

    code = ks_unmarshal_general_string_contents(context, in_str, *out_data);
    if (code != 0) {
        free(*out_data);
        *out_data = NULL;
        return code;
    }

    return 0;
}
#endif

krb5_error_code
ks_unmarshal_Principal(krb5_context context,
                       const Principal *hprinc,
                       krb5_principal *out_kprinc)
{
    krb5_error_code code;
    krb5_principal kprinc;
    unsigned int i;

    kprinc = k5alloc(sizeof(*kprinc), &code);
    if (code != 0)
        return code;

    kprinc->magic = KV5M_PRINCIPAL;
    kprinc->type = hprinc->name.name_type;
    kprinc->data = k5alloc(hprinc->name.name_string.len * sizeof(krb5_data),
                           &code);
    if (code != 0) {
        krb5_free_principal(context, kprinc);
        return code;
    }
    for (i = 0; i < hprinc->name.name_string.len; i++) {
        code = ks_unmarshal_general_string_contents(context,
                                                    hprinc->name.name_string.val[i],
                                                    &kprinc->data[i]);
        if (code != 0) {
            krb5_free_principal(context, kprinc);
            return code;
        }
        kprinc->length++;
    }
    code = ks_unmarshal_general_string_contents(context,
                                                hprinc->realm,
                                                &kprinc->realm);
    if (code != 0) {
        krb5_free_principal(context, kprinc);
        return code;
    }

    *out_kprinc = kprinc;

    return 0;
}

static krb5_error_code
ks_unmarshal_Event(krb5_context context,
                   const Event *event,
                   krb5_db_entry *kentry)
{
    krb5_error_code code;
    krb5_principal princ = NULL;

    if (event->principal != NULL) {
        code = ks_unmarshal_Principal(context, event->principal, &princ);
        if (code != 0)
            return code;
    }

    code = krb5_dbe_update_mod_princ_data(context, kentry,
                                          event->time, princ);

    krb5_free_principal(context, princ);

    return code;
}

static krb5_error_code
ks_unmarshal_HDBFlags(krb5_context context,
                      HDBFlags hflags,
                      krb5_flags *kflags)
{
    *kflags = 0;

    if (hflags.initial)
        *kflags |= KRB5_KDB_DISALLOW_TGT_BASED;
    if (!hflags.forwardable)
        *kflags |= KRB5_KDB_DISALLOW_FORWARDABLE;
    if (!hflags.proxiable)
        *kflags |= KRB5_KDB_DISALLOW_PROXIABLE;
    if (!hflags.renewable)
        *kflags |= KRB5_KDB_DISALLOW_RENEWABLE;
    if (!hflags.postdate)
        *kflags |= KRB5_KDB_DISALLOW_POSTDATED;
    if (!hflags.server)
        *kflags |= KRB5_KDB_DISALLOW_SVR;
    if (hflags.client)
        ;
    if (hflags.invalid)
        *kflags |= KRB5_KDB_DISALLOW_ALL_TIX;
    if (hflags.require_preauth)
        *kflags |= KRB5_KDB_REQUIRES_PRE_AUTH;
    if (hflags.change_pw)
        *kflags |= KRB5_KDB_PWCHANGE_SERVICE;
    if (hflags.require_hwauth)
        *kflags |= KRB5_KDB_REQUIRES_HW_AUTH;
    if (hflags.ok_as_delegate)
        *kflags |= KRB5_KDB_OK_AS_DELEGATE;
    if (hflags.user_to_user)
        ;
    if (hflags.immutable)
        ;
    if (hflags.trusted_for_delegation)
        *kflags |= KRB5_KDB_OK_TO_AUTH_AS_DELEGATE;
    if (hflags.allow_kerberos4)
        ;
    if (hflags.allow_digest)
        ;
    return 0;
}

static krb5_error_code
ks_unmarshal_Key(krb5_context context,
                 const hdb_entry *hentry,
                 const Key *hkey,
                 krb5_key_data *kkey)
{
    memset(kkey, 0, sizeof(*kkey));

    kkey->key_data_ver = KRB5_KDB_V1_KEY_DATA_ARRAY;
    kkey->key_data_kvno = hentry->kvno;

    kkey->key_data_type[0] = hkey->key.keytype;
    kkey->key_data_contents[0] = malloc(hkey->key.keyvalue.length);
    if (kkey->key_data_contents[0] == NULL)
        return ENOMEM;

    memcpy(kkey->key_data_contents[0], hkey->key.keyvalue.data,
           hkey->key.keyvalue.length);
    kkey->key_data_length[0] = hkey->key.keyvalue.length;

    if (hkey->salt != NULL) {
        switch (hkey->salt->type) {
        case hdb_pw_salt:
            kkey->key_data_type[1] = KRB5_KDB_SALTTYPE_NORMAL;
            break;
        case hdb_afs3_salt:
            kkey->key_data_type[1] = KRB5_KDB_SALTTYPE_AFS3;
            break;
        default:
            kkey->key_data_type[1] = KRB5_KDB_SALTTYPE_SPECIAL;
            break;
        }

        kkey->key_data_contents[1] = malloc(hkey->salt->salt.length);
        if (kkey->key_data_contents[1] == NULL) {
            memset(kkey->key_data_contents[0], 0, kkey->key_data_length[0]);
            free(kkey->key_data_contents[0]);
            return ENOMEM;
        }
        memcpy(kkey->key_data_contents[1], hkey->salt->salt.data,
               hkey->salt->salt.length);
        kkey->key_data_length[1] = hkey->salt->salt.length;
    }

    return 0;
}

/*
 * Extension marshalers
 */

static krb5_error_code
ks_unmarshal_HDB_extension_data_last_pw_change(krb5_context context,
                                               HDB_extension *hext,
                                               krb5_db_entry *kentry)
{
    return krb5_dbe_update_last_pwd_change(context, kentry,
                                           hext->data.u.last_pw_change);
}

typedef krb5_error_code (*ks_hdb_marshal_extension_fn)(krb5_context,
                                                       const krb5_db_entry *,
                                                       HDB_extension *);

typedef krb5_error_code (*ks_hdb_unmarshal_extension_fn)(krb5_context,
                                                         HDB_extension *,
                                                         krb5_db_entry *);

struct {
    ks_hdb_unmarshal_extension_fn unmarshal;
} ks_hdb_extension_vtable[] = {
    { NULL }, /* choice_HDB_extension_data_asn1_ellipsis */
    { NULL }, /* choice_HDB_extension_data_pkinit_acl */
    { NULL }, /* choice_HDB_extension_data_pkinit_cert_hash */
    { NULL }, /* choice_HDB_extension_data_allowed_to_delegate_to */
    { NULL }, /* choice_HDB_extension_data_lm_owf */
    { NULL }, /* choice_HDB_extension_data_password */
    { NULL }, /* choice_HDB_extension_data_aliases */
    { ks_unmarshal_HDB_extension_data_last_pw_change }
};

static const size_t ks_hdb_extension_count =
    sizeof(ks_hdb_extension_vtable) / sizeof(ks_hdb_extension_vtable[0]);

static krb5_error_code
ks_unmarshal_HDB_extension(krb5_context context,
                           HDB_extension *hext,
                           krb5_db_entry *kentry)
{
    ks_hdb_unmarshal_extension_fn unmarshal = NULL;

    if (hext->data.element < ks_hdb_extension_count)
        unmarshal = ks_hdb_extension_vtable[hext->data.element].unmarshal;

    if (unmarshal == NULL)
        return hext->mandatory ? KRB5_KDB_DBTYPE_NOSUP : 0;

    return (*unmarshal)(context, hext, kentry);
}

static krb5_error_code
ks_unmarshal_HDB_extensions(krb5_context context,
                            HDB_extensions *hexts,
                            krb5_db_entry *kentry)
{
    unsigned int i;
    krb5_error_code code = 0;

    for (i = 0; i < hexts->len; i++) {
        code = ks_unmarshal_HDB_extension(context, &hexts->val[i], kentry);
        if (code != 0)
            break;
    }

    return code;
}

krb5_error_code
ks_unmarshal_hdb_entry(krb5_context context,
                       struct hdb_entry_ex *hentry_ex,
                       krb5_db_entry **kentry_ptr)
{
    const hdb_entry *hentry = &hentry_ex->entry;
    krb5_db_entry *kentry;
    krb5_error_code code;
    unsigned int i;

    *kentry_ptr = NULL;

    kentry = k5alloc(sizeof(*kentry), &code);
    if (kentry == NULL) {
        return code;
    }

    kentry->magic = KRB5_KDB_MAGIC_NUMBER;
    kentry->len = KRB5_KDB_V1_BASE_LENGTH;

    code = ks_unmarshal_Principal(context, hentry->principal, &kentry->princ);
    if (code != 0) {
        goto cleanup;
    }

    code = ks_unmarshal_HDBFlags(context, hentry->flags, &kentry->attributes);
    if (code != 0) {
        goto cleanup;
    }

    if (hentry->max_life != NULL)
        kentry->max_life = *(hentry->max_life);
    if (hentry->max_renew != NULL)
        kentry->max_renewable_life = *(hentry->max_renew);
    if (hentry->valid_end != NULL)
        kentry->expiration = *(hentry->valid_end);
    if (hentry->pw_end != NULL)
        kentry->pw_expiration = *(hentry->pw_end);

    /* last_success */
    /* last_failed */
    /* fail_auth_count */
    /* n_tl_data */

    code = ks_unmarshal_Event(context,
                              hentry->modified_by ? hentry->modified_by :
                              &hentry->created_by,
                              kentry);
    if (code != 0) {
        goto cleanup;
    }

    if (hentry->extensions) {
        code = ks_unmarshal_HDB_extensions(context, hentry->extensions, kentry);
        if (code != 0) {
            goto cleanup;
        }
    }

    kentry->key_data = k5alloc(hentry->keys.len * sizeof(krb5_key_data), &code);
    if (code != 0) {
        goto cleanup;
    }

    for (i = 0; i < hentry->keys.len; i++) {
        code = ks_unmarshal_Key(context, hentry,
                                &hentry->keys.val[i],
                                &kentry->key_data[i]);
        if (code != 0) {
            goto cleanup;
        }

        kentry->n_key_data++;
    }

    kentry->e_data = (void *)hentry_ex;

    *kentry_ptr = kentry;

cleanup:
    if (code != 0) {
        ks_free_krb5_db_entry(context, kentry);
    }
    return code;
}
